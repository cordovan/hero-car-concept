//
//  AppDelegate.h
//  Hero Car Concept
//
//  Created by Jens Hendar on 17/08/15.
//  Copyright (c) 2015 Cordovan Communications. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


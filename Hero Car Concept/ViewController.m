//
//  ViewController.m
//  Hero Car Concept
//
//  Created by Jens Hendar on 17/08/15.
//  Copyright (c) 2015 Cordovan Communications. All rights reserved.
//

#import "ViewController.h"
//#import "EVURLCache.h"
//#import "Reachability.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //NSURL *url = [NSURL URLWithString:@""];
    // Determile cache file path
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0],@"index.html"];
    NSURL *url = [NSURL URLWithString:@"http://ipad.volvo.tripnet.se/vcc/192732/24449/3927056fgfjhj.aspx"];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    [urlData writeToFile:filePath atomically:YES];
    [mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]]];*/
    
    
    NSString *stringurl=[NSString stringWithFormat:@"http://stage2.cordovan.se/webb_labb/hero_car_concept/first/web/index.html"]; //
    NSURL *url=[NSURL URLWithString:stringurl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
    [mainWebView loadRequest:theRequest];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
